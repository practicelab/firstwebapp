import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NancyNotesComponent } from './nancy-notes.component';

describe('NancyNotesComponent', () => {
  let component: NancyNotesComponent;
  let fixture: ComponentFixture<NancyNotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NancyNotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NancyNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
